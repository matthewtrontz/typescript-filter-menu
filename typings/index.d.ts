type Movie = {
  title: string;
  year: number;
  votes: number;
  genreIds: number[];
  description: string;
};

type Genre = {
  id: number;
  name: string;
};

type MoviesProps = {
  movies: Movie[];
  genres: Genre[];
};

type Function = (movie: Movie) => boolean;

type Filter = {
  name: string | number;
  group: Group;
  fnc: Function;
};
